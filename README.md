> **此仓库是Solitude的示例站，包含初步配置主题文件、新建相关页面，达到一个不需要复杂的配置即可实现Heo的效果。目的在于帮助小白快速建站。**

> Solitude主题预览：[青桔气球](https://blog.qjqq.cn)

# 安装hexo框架

你需要到到[Hexo 官方文档](https://hexo.io/zh-cn/docs/#%E5%AE%89%E8%A3%85)去安装，他会告诉你如何进行安装、建站。

# 安装Solitude主题

[Solitude 官方文档](https://solitude.js.org/zh/guide/quick-start)

## 安装主题
```shell
git clone -b main https://github.com/everfu/hexo-theme-solitude.git themes/solitude
 ```
## 应用主题
修改 hexo 根目录配置文件 `_config.yml`，把主题改为 `solitude`。
```yaml
theme: solitude
```
## 安装依赖
主题使用了 `Pug` 与 `Stylus`，需要额外安装各自的渲染器。
```shell
npm install hexo-renderer-pug hexo-renderer-stylus --save
```
## 语言配置
修改站点配置文件 `_config.yml`（不是主题配置文件）。
支持语言：en-US (美式英文) 、 zh-CN (简体中文)、zh-TW (繁体中文)
```yaml
language: zh-CN # 语言
```
## 优化配置
在博客根目录运行以下命令，将主题的配置文件复制到根目录，以便更好地配置主题。

Windows
```shell
copy .\themes\solitude\_config.yml .\_config.solitude.yml
```
Mac/Linux
```shell
cp -rf ./themes/solitude/_config.yml ./_config.solitude.yml
```
1. [x] 根目录的 `_config.solitude.yml` 的配置都是高优先级，因此，渲染时会优先采用此文件的配置项内容。
1. [x] 在更新主题时可能会存在配置变更，请注意更新说明，可能需要手动对 `_config.solitude.yml` 同步修改。

# 安装所需插件
这份主题是经过`配置`的，所以你只需要安装好所需插件就可以顺利运行了。

1. 配置文章链接转数字或字母(_config.yml)
    ```shell
    npm install hexo-abbrlink --save
    ```
2. 本地搜索依赖(_config.yml/_config.solitude.yml)
    ```shell
    npm install hexo-generator-search --save
    ```
3. 字数统计(_config.solitude.yml)
    ```shell
    npm install hexo-wordcount --save
    ```
4. SWPP(_config.yml)
```shell
npm install hexo-swpp swpp-backends --save
```
5. Gulp
```shell
npm install gulp compress gulp-clean-css gulp-html-minifier-terser gulp-htmlclean gulp-terser --save-dev
```
6. Tag Plugins
```shell
npm install hexo-solitude-tag
```